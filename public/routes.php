<?php

$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) {
    //account block
    $r->addRoute('GET', '/', 'App\Controllers\AccountController@index');
    $r->addRoute('GET', '/account', 'App\Controllers\AccountController@index');
    $r->addRoute('POST', '/account/login', 'App\Controllers\AccountController@login');
    $r->addRoute('POST', '/account/registration', 'App\Controllers\AccountController@registration');
    $r->addRoute(['GET','POST'], '/account/editpartnerdata', 'App\Controllers\AccountController@editPartnerData');
    $r->addRoute('GET', '/account/info', 'App\Controllers\AccountController@info');
    $r->addRoute('POST', '/account/linkgeneration', 'App\Controllers\AccountController@linkgeneration');
    //info block
    $r->addRoute('GET', '/info', 'App\Controllers\InfoController@index');
    $r->addRoute('GET', '/info/bookkeeping', 'App\Controllers\InfoController@bookkeeping');
    $r->addRoute('GET', '/info/orderstatistics', 'App\Controllers\InfoController@orderStatistics');
    $r->addRoute('GET', '/info/promotionalmaterials', 'App\Controllers\InfoController@promoMaterials');
    $r->addRoute('GET', '/generalstatistics', 'App\Controllers\InfoController@generalStatistics');
});
