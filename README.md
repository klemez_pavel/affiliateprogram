# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Test task for affiliate program.

### How do I get set up? ###

Runs php7 and apache2.

```bash
composer install 
```
Set apache config:

```config
<VirtualHost affiliate.dev:80>
    ServerName affiliate.dev
    ServerAdmin affiliate.dev
    DocumentRoot "Project public folder root"  
    <Directory "Project public folder root">
          RewriteEngine on

          # Если запрашиваемая в URL директория или файл существуют обращаемся к ним напрямую
          RewriteCond %{REQUEST_FILENAME} !-f
          RewriteCond %{REQUEST_FILENAME} !-d
          # Если нет - перенаправляем запрос на index.php
          RewriteRule . index.php

          Options Indexes FollowSymLinks
          AllowOverride All
          Require all granted
    </Directory>
</VirtualHost>
```

