<?php

namespace App\Services;

use App\Interfaces\AccountInterface;
use GuzzleHttp\Client;

class AccountService extends HttpService implements AccountInterface
{

    public function __construct()
    {
        $this->httpClient = new Client();
    }

    /**
     * @param $email
     * @param $password
     * @return mixed
     */
    public function login($email, $password)
    {
        $response = $this->httpClient->request('POST', self::BaseUri . '/account/login', [
            'auth' => [
                $this->apiUsername,
                $this->apiPassword
            ],
            'json' => [
                'email' => $email,
                'password' => $password,
            ],
        ])->getBody();
        return json_decode($response, true);
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function registration(array $params)
    {
        $response = $this->httpClient->request('POST', self::BaseUri . '/account/registration', [
            'auth' => [
                $this->apiUsername,
                $this->apiPassword
            ],
            'body' => \GuzzleHttp\json_encode($params),
        ])->getBody();
        return json_decode($response, true);
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function editPartnerData(array $params)
    {
        $response = $this->httpClient->request('POST', self::BaseUri . '/account/editpartnerdata', [
            'auth' => [
                $this->apiUsername,
                $this->apiPassword
            ],
            'body' => json_encode($params)
        ])->getBody();
        return json_decode($response, true);
    }

    /**
     * @param $idsession
     * @return mixed
     */
    public function info($idsession)
    {
        $response = $this->httpClient->request('POST', self::BaseUri . '/account/info', [
            'auth' => [
                $this->apiUsername,
                $this->apiPassword
            ],
            'json' => [
                'idsession' => $idsession,
            ]
        ])->getBody();

        return json_decode($response, true);
    }

    /**
     * @param $idsession
     * @param $link
     * @return mixed
     */
    public function linkGeneration($idsession, $link)
    {
        $response = $this->httpClient->request('POST', self::BaseUri . '/account/linkgeneration', [
            'auth' => [
                $this->apiUsername,
                $this->apiPassword
            ],
            'json' => [
                'idsession' => $idsession,
                'affiliatelink' => $link
            ]
        ])->getBody();
        return json_decode($response ,true);
    }
}