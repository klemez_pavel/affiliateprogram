<?php

namespace App\Services;

use App\Interfaces\InfoInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Created by PhpStorm.
 * User: kleme
 * Date: 14.09.2017
 * Time: 16:12
 */
class InfoService extends HttpService implements InfoInterface
{
    public function __construct()
    {
        $this->httpClient = new Client();
    }

    public function bookkeeping($idsession)
    {
        $response = $this->httpClient->request('POST', self::BaseUri . '/info/bookkeeping', [
            'auth' => [
                $this->apiUsername,
                $this->apiPassword
            ],
            'json' => [
                'idsession' => $idsession,
            ]
        ])->getBody();
        return json_decode($response, true);
    }

    public function promotionalMaterials($idsession)
    {

        $response = $this->httpClient->request('POST', self::BaseUri . '/info/promotionalmaterials', [
            'auth' => [
                $this->apiUsername,
                $this->apiPassword
            ],
            'json' => [
                'idsession' => $idsession,
            ]
        ])->getBody();
        return json_decode($response, true);
    }


    public function orderStatistics($idsession)
    {
        $response = $this->httpClient->request('POST', self::BaseUri . '/info/orderstatistics', [
            'auth' => [
                $this->apiUsername,
                $this->apiPassword
            ],
            'json' => [
                'idsession' => $idsession,
            ]
        ])->getBody();
        return json_decode($response,true);
    }

    public function generalStatistics()
    {
        $response = $this->httpClient->request('GET', self::BaseUri . '/generalstatistics',[
            'auth' => [
                $this->apiUsername,
                $this->apiPassword
            ],
        ])->getBody();
        return json_decode($response, true);
    }
}