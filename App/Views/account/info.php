<?php include_once(dirname(__FILE__) . '/../layouts/head.php'); ?>
<div class="container">
    <?php include_once(dirname(__FILE__) . '/../layouts/navbar.php'); ?>
    <h3 class="display-4">Информация об аккаунте</h3>
    <?php if (isset($data)): ?>
        <dl class="row">
            <dt class="col-sm-3">Email</dt>
            <dd class="col-sm-9"><?= htmlspecialchars($data['email']) ?></dd>
            <dt class="col-sm-3">Имя</dt>
            <dd class="col-sm-9"><?= htmlspecialchars($data['name']) ?></dd>
            <dt class="col-sm-3">Фамилия</dt>
            <dd class="col-sm-9"><?= htmlspecialchars($data['surname']) ?></dd>
            <dt class="col-sm-3">Отчество</dt>
            <dd class="col-sm-9"><?= htmlspecialchars($data['patronymic']) ?></dd>
            <dt class="col-sm-3">Город</dt>
            <dd class="col-sm-9"><?= htmlspecialchars($data['city']) ?></dd>
            <dt class="col-sm-3">Сайт</dt>
            <dd class="col-sm-9"><?= htmlspecialchars($data['site']) ?></dd>
            <dt class="col-sm-3">Способ вывода средств</dt>
            <dd class="col-sm-9"><?= htmlspecialchars($data['card']) ?></dd>
        </dl>
        <a href="/account/editpartnerdata">
            <button>Редактировать</button>
        </a>
    <?php endif; ?>
</div>
<?php include_once(dirname(__FILE__) . '/../layouts/jsScripts.php'); ?>
<?php include_once(dirname(__FILE__) . '/../layouts/footer.php'); ?>
