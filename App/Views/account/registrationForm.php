<div>
    <form id="registrationForm" method="POST" action="/account/registration" role="form">
        <div class="form-row">
            <div class="form-group col-md-3">
                <label for="email" class="col-form-label">Email</label>
                <input type="text" class="form-control" id="email" name="email" required>
            </div>
            <div class="form-group col-md-3">
                <label for="password" class="col-form-label">Пароль</label>
                <input type="password" class="form-control" id="registrationPassword" name="password" required>
            </div>
            <div class="form-group col-md-3">
                <label for="confirmPassword" class="col-form-label">Подтверждение пароля</label>
                <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" required>
                <div class="help-block" id="passwordValidate"></div>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-3">
                <label for="name" class="col-form-label">Имя</label>
                <input type="text" class="form-control" id="name" name="name" required>
            </div>
            <div class="form-group col-md-3">
                <label for="surname" class="col-form-label">Фамилия</label>
                <input type="text" class="form-control" id="surname" name="surname" required>
            </div>
            <div class="form-group col-md-3">
                <label for="patronymic" class="col-form-label">Отчество</label>
                <input type="text" class="form-control" id="patronymic" name="patronymic" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-3">
                <label for="city" class="col-form-label">Город</label>
                <input type="text" class="form-control" id="city" name="city" required>
            </div>

            <div class="form-group col-md-3">
                <label for="site" class="col-form-label">Сайт</label>
                <input type="text" class="form-control" id="site" name="site" required>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Зарегистрироваться</button>
    </form>
    <?php if (isset($registrationError)): ?>
        <div>
            <ul>
                <li class="alert alert-danger"><?= htmlspecialchars($registrationError) ?></li>
            </ul>
        </div>
    <?php endif ?>
</div>
