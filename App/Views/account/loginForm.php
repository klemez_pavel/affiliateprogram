<div>
    <!-- @todo add email validity check-->
    <form method="POST" action="/account/login">
        <div class="form-group col-md-4">
            <label for="email" class="col-form-label">Email</label>
            <input type="email" class="form-control" id="email" name="email" required>
        </div>
        <div class="form-group col-md-4">
            <label for="password" class="col-form-label">Пароль</label>
            <input type="password" class="form-control" id="loginPassword" name="password" required>
        </div>
        <button type="submit" class="btn btn-primary">Войти</button>
    </form>

    <div>
        <?php if(isset($loginError)): ?>
        <ul>
            <li class="alert alert-danger"><?= htmlspecialchars($loginError) ?></li>
        </ul>
        <?php endif ?>
    </div>

</div>