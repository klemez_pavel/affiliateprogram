<?php include_once(dirname(__FILE__) . '/../layouts/head.php'); ?>
<div class="container">
    <?php include_once(dirname(__FILE__) . '/../layouts/navbar.php'); ?>
    <form method="POST" action="/account/editpartnerdata">
        <div class="form-row">
            <div class="form-group col-md-3">
                <label for="email" class="col-form-label">Email</label>
                <input type="text" class="form-control" id="email" name="email"
                       value="<?= htmlspecialchars($formData['email']) ?>" required>
            </div>
            <div class="form-group col-md-3">
                <label for="password" class="col-form-label">Пароль</label>
                <input type="password" class="form-control" id="password" name="password">
            </div>
            <div class="form-group col-md-3">
                <label for="oldPassword" class="col-form-label">Старый пароль</label>
                <input type="password" class="form-control" id="oldPassword" name="oldPassword">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-3">
                <label for="name" class="col-form-label">Имя</label>
                <input type="text" class="form-control" id="name" name="name"
                       value="<?= htmlspecialchars($formData['name']) ?>" required>
            </div>
            <div class="form-group col-md-3">
                <label for="surname" class="col-form-label">Фамилия</label>
                <input type="text" class="form-control" id="surname" name="surname"
                       value="<?= htmlspecialchars($formData['surname']) ?>" required>
            </div>
            <div class="form-group col-md-3">
                <label for="patronymic" class="col-form-label">Отчество</label>
                <input type="text" class="form-control" id="patronymic" name="patronymic"
                       value="<?= htmlspecialchars($formData['patronymic']) ?>" required>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-3">
                <label for="site" class="col-form-label">Карта</label>
                <input type="text" class="form-control" id="site" name="site"
                       readonly value="<?= htmlspecialchars($formData['card'])?>" >
            </div>
            <div class="form-group col-md-3">
                <label for="city" class="col-form-label">Город</label>
                <input type="text" class="form-control" id="city" name="city"
                      value="<?= htmlspecialchars($formData['city']) ?>" required>
            </div>

            <div class="form-group col-md-3">
                <label for="site" class="col-form-label">Сайт</label>
                <input type="text" class="form-control" id="site" name="site"
                       value="<?= htmlspecialchars($formData['site']) ?>" required>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Редактировать</button>
    </form>
    <div>
        <?php if (isset($editError)): ?>
            <ul>
                <li class="alert alert-danger"><?= htmlspecialchars($editError) ?></li>
            </ul>
        <?php endif ?>
    </div>
</div>
<?php include_once(dirname(__FILE__) . '/../layouts/jsScripts.php'); ?>
<script>
    $(function () {
        $('#password').change(function () {
            if(this.value.length != 0) {
                $('#oldPassword').prop('required', true);
            } else {
                $('#oldPassword').prop('required', false);
            }
        })
        $('#oldPassword').change(function () {
            if(this.value.length != 0) {
                $('#password').prop('required', true);
            } else {
                $('#password').prop('required', false);
            }
        })
    })
</script>
<?php include_once(dirname(__FILE__) . '/../layouts/footer.php'); ?>

