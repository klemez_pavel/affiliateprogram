<?php include_once(dirname(__FILE__) . '/../layouts/head.php'); ?>
<div class="container">
    <?php include_once(dirname(__FILE__) . '/../layouts/navbar.php'); ?>
    <h3 class="display-4">Выплаты</h3>
    <!-- Add warning if data empty -->
    <?php if ($data['payments']): ?>
        <table class="table">
            <thead>
            <th>Дата</th>
            <th>Сумма</th>
            <th>Выплачено</th>
            <th>Комментарий</th>
            </thead>
            <tbody>
            <?php foreach ($data['payments'] as $payment): ?>
                <tr>
                    <td><?= htmlspecialchars(date("Y-m-d H:i:s",strtotime($payment['date']))) ?></td>
                    <td><?= htmlspecialchars($payment['sum']) ?></td>
                    <td><?= htmlspecialchars($payment['paid'] ? "Да" : "Нет") ?></td>
                    <td><?= htmlspecialchars($payment['comment']) ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif ?>
</div>
<?php include_once(dirname(__FILE__) . '/../layouts/jsScripts.php'); ?>
<?php include_once(dirname(__FILE__) . '/../layouts/footer.php'); ?>
