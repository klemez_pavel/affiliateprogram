<?php include_once(dirname(__FILE__) . '/../layouts/head.php'); ?>
<div class="container">
    <?php include_once(dirname(__FILE__) . '/../layouts/navbar.php'); ?>

    <div>
        <?php if (isset($error)): ?>
            <ul>
                <li class="alert alert-danger"><?= htmlspecialchars($error) ?></li>
            </ul>
        <?php endif ?>
    </div>
</div>
<?php include_once(dirname(__FILE__) . '/../layouts/jsScripts.php'); ?>
<?php include_once(dirname(__FILE__) . '/../layouts/footer.php'); ?>
