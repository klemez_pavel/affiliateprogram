<?php include_once(dirname(__FILE__) . '/../layouts/head.php'); ?>
<div class="container">
    <?php include_once(dirname(__FILE__) . '/../layouts/navbar.php'); ?>
    <h3 class="display-4">Купоны и ссылки</h3>
    <p>Сгенерировать ссылку</p>
    <form action="" method="POST" id="linkGenerateForm">
        <div class="form-group">
            <input class="form-control" type="text" id="affiliate_link" name="affiliate_link" required>
        </div>
        <button type="submit" class="btn btn-primary">Получить</button>
    </form>
    <br>
    <div class="col-xs-12">
        Ссылка:
        <span id="affiliate_link_utm">
            <input class="form-control" type="text">
        </span>
    </div>
    <br>
    <?php foreach ($data['promotionalmaterials'] as $promoMaterial) : ?>
        <dl class="row">
            <dt class="col-sm-3">Название программы</dt>
            <dd class="col-sm-9"><?= htmlspecialchars($promoMaterial['name_of_program']) ?></dd>

            <dt class="col-sm-3">Описание программы</dt>
            <dd class="col-sm-9"><?= htmlspecialchars($promoMaterial['program_description']) ?></dd>

            <dt class="col-sm-3">Комиссия за первый заказ</dt>
            <dd class="col-sm-9"><?= htmlspecialchars($promoMaterial['commission_for_first_order']) ?></dd>

            <dt class="col-sm-3">Комиссия за повторный заказ</dt>
            <dd class="col-sm-9"><?= htmlspecialchars($promoMaterial['commission_for_reordering']) ?></dd>
            <?php if(strlen($promoMaterial['coupon_code']) != 0) :  ?>
            <dt class="col-sm-3">Код купона</dt>
            <dd class="col-sm-9"><?= htmlspecialchars($promoMaterial['coupon_code']) ?></dd>
            <?php endif;?>
        </dl>
        <hr>
    <?php endforeach; ?>
</div>
<?php include_once(dirname(__FILE__) . '/../layouts/jsScripts.php'); ?>
<script>
    //@todo add removing '?' sign? or do it on backend.
    $(function () {
        $('#linkGenerateForm').submit(function (e) {
            e.preventDefault();
            $.ajax(
                {
                    method: 'POST',
                    url: '/account/linkgeneration',
                    data: {
                        affiliate_link: $('#affiliate_link').val()
                    }
                }
            )
            .done(function (html) {
                $('#affiliate_link_utm input').val("").val(html).select();
            })
            .fail(function (error) {
                if ($('#generateError').length) {
                    $('#generateError').remove();
                }
                $('#linkGenerateForm')
                    .after('<div id="generateError" class="alert alert-danger" style="margin-top: 5px"> '
                        + error.responseText + ' </div>');

            })
        })
    })
</script>
<?php include_once(dirname(__FILE__) . '/../layouts/footer.php'); ?>
