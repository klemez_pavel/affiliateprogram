<?php include_once(dirname(__FILE__) . '/../layouts/head.php'); ?>
<div class="container">
    <?php include_once(dirname(__FILE__) . '/../layouts/navbar.php'); ?>
    <?php if (array_key_exists('statistics', $data)): ?>
        <h3 class="display-4">Общая статистика</h3>
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>ФИО</th>
                <th>Сумма заказов</th>
                <th>Сумма вознаграждения</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data['statistics'] as $statistic): ?>
                <tr>
                    <td><?= htmlspecialchars($statistic['fio']) ?></td>
                    <td><?= htmlspecialchars($statistic['sum_of_sales']) ?></td>
                    <td><?= htmlspecialchars($statistic['amount_of_remuneration']) ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
    <div>
        <?php if (isset($error)): ?>
            <ul>
                <li class="alert alert-danger"><?= htmlspecialchars($error) ?></li>
            </ul>
        <?php endif ?>
    </div>
</div>
<?php include_once(dirname(__FILE__) . '/../layouts/jsScripts.php'); ?>
<?php include_once(dirname(__FILE__) . '/../layouts/footer.php'); ?>
