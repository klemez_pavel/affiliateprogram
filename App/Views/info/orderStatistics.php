<?php include_once(dirname(__FILE__) . '/../layouts/head.php'); ?>
<div class="container">
    <?php include_once(dirname(__FILE__) . '/../layouts/navbar.php'); ?>
    <?php $i=0;?>
    <h3 class="display-4">Заказы партнёра</h3>
    <table class="table">
        <thead>
        <tr>
            <th>Дата заказа</th>
            <th>Номер заказа</th>
            <th>Сумма заказа</th>
            <th>Сумма вознаграждения</th>
            <th>Статус заказа</th>
            <th>Причина отказа</th>
            <th>Новый клиент</th>
            <th>Название программы</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data['orders'] as $order): ?>
            <?php $i++; ?>
            <tr>
                <td><?= htmlspecialchars(date("Y-m-d H:i:s",strtotime($order['order_date']))) ?></td>
                <td><?= htmlspecialchars($order['order_number']) ?></td>
                <td><?= htmlspecialchars($order['sum_of_sale']) ?></td>
                <td><?= htmlspecialchars($order['amount_of_remuneration']) ?></td>
                <td><?= htmlspecialchars($order['order_status']) ?></td>
                <td><?= htmlspecialchars($order['rejection_reason']) ?></td>
                <td><?= $order['new_client'] ? 'Да' : 'Нет' ?></td>
                <td><?= htmlspecialchars($order['name_program']) ?></td>
                <td>
                    <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample<?=$i?>"
                       aria-expanded="false" aria-controls="collapseExample<?=$i?>">
                        Показать
                    </a>
                </td>
            </tr>
            <tr class="collapse" id="collapseExample<?=$i?>">
                <td colspan="<?= count($order) ?>">
                    <div class="card card-body">
                        <?php foreach ($order['products'] as $product): ?>
                            <dl>
                                <dt>Имя</dt>
                                <dd><?= htmlspecialchars($product['product']); ?></dd>
                                <dt>Свойство</dt>
                                <dd><?= htmlspecialchars($product['product_property']) ?></dd>
                                <dt>Количество</dt>
                                <dd> <?= htmlspecialchars($product['count']) ?></dd>
                                <dt>Цена</dt>
                                <dd><?= htmlspecialchars($product['price']) ?></dd>
                                <dt>Сумма</dt>
                                <dd> <?= htmlspecialchars($product['summ']) ?></dd>
                            </dl>
                        <?php endforeach; ?>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php include_once(dirname(__FILE__) . '/../layouts/jsScripts.php'); ?>
<?php include_once(dirname(__FILE__) . '/../layouts/footer.php'); ?>
