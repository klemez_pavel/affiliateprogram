<?php include_once('layouts/head.php'); ?>
<div class="container">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="login-tab" data-toggle="tab" href="#login"
               role="tab" aria-controls="login" aria-expanded="true">Войти</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="registration-tab" data-toggle="tab" href="#registration"
               role="tab" aria-controls="registration">Зарегистрироваться</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane active" id="login" role="tabpanel">
            <?php include_once('account/loginForm.php'); ?>
        </div>
        <div class="tab-pane" id="registration" role="tabpanel">
            <?php include_once('account/registrationForm.php') ?>
        </div>
    </div>
</div>
<?php include_once('layouts/jsScripts.php'); ?>
<script>
    $('#registrationForm').on('submit', function () {
        console.log('1');
        if($('#registrationPassword').val() === $('#confirmPassword').val()) {
            $('#passwordValidate').hide();
            return true;
        }
        $('#passwordValidate').html('').append('Пароли не совпадают').show();
        return false;
    })
</script>
<?php include_once('layouts/footer.php')?>
