<?php
/**
 * Created by PhpStorm.
 * User: kleme
 * Date: 15.09.2017
 * Time: 9:31
 */

namespace App\Interfaces;


interface AccountInterface
{
        public function login($email, $password);

        public function registration(array $params);

        public function editPartnerData(array $params);

        public function info($idsession);

        public function linkGeneration($idsession, $link);
}