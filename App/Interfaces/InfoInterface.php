<?php
/**
 * Created by PhpStorm.
 * User: kleme
 * Date: 15.09.2017
 * Time: 13:27
 */

namespace App\Interfaces;


interface InfoInterface
{
    public function bookkeeping($idsession);

    public function orderStatistics($idsession);

    public function promotionalMaterials($idsession);

    public function generalStatistics();
}