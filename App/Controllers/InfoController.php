<?php

namespace App\Controllers;

use App\Services\InfoService;
use GuzzleHttp\Exception\RequestException;

class InfoController
{
    private $info;

    public function __construct()
    {
        $this->info = new InfoService();
    }

    public function index()
    {
        $viewName = 'info/index.php';
        if(!isset($_SESSION['idsession'])) {
            $error = 'Session error. Please login';
        }
        include_once('../App/Views/' . $viewName);
    }

    public function bookkeeping()
    {
        if (isset($_SESSION['idsession'])) {
            try {
                $data = $this->info->bookkeeping($_SESSION['idsession']);
                $viewName = 'info/bookkeeping.php';
            } catch (RequestException $e) {
                if($e->hasResponse()) {
                    $responseBody = json_decode($e->getResponse()->getBody());
                    $error = $e->getCode() . ' ' . $responseBody->errors[0];
                }
                $viewName = 'info/index.php';
            }
        } else {
            $viewName = 'info/index.php';
            $error = 'Session error. Please refresh the page';
        }
        include_once('../App/Views/' . $viewName);
    }

    public function orderStatistics()
    {
        if (isset($_SESSION['idsession'])) {
            try {
                $viewName = 'info/orderStatistics.php';
                $data = $this->info->orderStatistics($_SESSION['idsession']);
            } catch (RequestException $e) {
                if($e->hasResponse()) {
                    $responseBody = json_decode($e->getResponse()->getBody());
                    $error = $e->getCode() . ' ' . $responseBody->errors[0];
                }
                $viewName = 'info/index.php';
            }
        } else {
            $viewName = 'info/index.php';
            $error = 'Session error. Please refresh the page';
        }
        include_once('../App/Views/' . $viewName);
    }

    public function promoMaterials()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_SESSION['idsession'])) {
            try {
                $viewName = 'info/promotionalMaterials.php';
                $data = $this->info->promotionalMaterials($_SESSION['idsession']);
            } catch (RequestException $e) {
                if ($e->hasResponse()) {
                    $responseBody = json_decode($e->getResponse()->getBody());
                    $error = $responseBody->errors[0];
                }
                $viewName = 'info/index.php';
            }
        } else {
            $viewName = 'info/index.php';
            $error = 'Session error. Please refresh the page';
        }
        include_once('../App/Views/' . $viewName);
    }

    public function generalStatistics()
    {
        $viewName = 'info/generalStatistics.php';
        try {
            $data = $this->info->generalStatistics();
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $responseBody = json_decode($e->getResponse()->getBody());
                $error = $responseBody->errors[0];
            }
        }
        include_once('../App/Views/' . $viewName);
    }
}