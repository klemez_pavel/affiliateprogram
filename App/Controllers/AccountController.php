<?php

namespace App\Controllers;

use App\Services\AccountService;
use GuzzleHttp\Exception\RequestException;
use yii\db\Exception;

class AccountController
{
    private $account;

    public function __construct()
    {
        $this->account = new AccountService();
    }

    public function index()
    {
        if (isset($_SESSION['idsession'])) {
            $this->info();
        } else {
            include_once('../App/Views/welcome.php');
        }
    }

    public function login()
    {
        try {
            $data = $this->account->login(
                $_POST['email'],
                $_POST['password']
            );
            $_SESSION['idsession'] = $data['idsession'];
            $_SESSION['idpartner'] = $data['idpartner'];
            $this->info();
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $responseBody = json_decode($e->getResponse()->getBody());
                $loginError = $responseBody->errors[0];
            }
            include_once('../App/Views/welcome.php');
        }
    }

    public function registration()
    {
        try {
            $data = $this->account->registration($_POST);
            $_SESSION['idsession'] = $data['idsession'];
            $_SESSION['idpartner'] = $data['idpartner'];
            $this->info();
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $responseBody = json_decode($e->getResponse()->getBody());
                $registrationError = $e->getCode() . ' ' . $responseBody->errors[0];
            }
            $viewName = 'welcome.php';
            include_once('../App/Views/' . $viewName);
        }
    }

    public function editPartnerData()
    {
        $viewName = 'account/editForm.php';
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_SESSION['idsession'])) {
            try {
                $params = $_POST;
                $params['idsession'] = $_SESSION['idsession'];
                if (strlen($params['password']) == 0) unset($params['password']);
                if (strlen($params['oldPassword']) == 0) unset($params['oldPassword']);
                $this->account->editPartnerData($params);
                $this->info();
                exit();

            } catch
            (RequestException $e) {
                if ($e->hasResponse()) {
                    $responseBody = json_decode($e->getResponse()->getBody());
                    $editError = $e->getCode() . ' ' . $responseBody->errors[0];
                    $formData = $this->account->info($params['idsession']);
                    $viewName = 'account/editForm.php';
                }
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_SESSION['idsession'])) {
            $formData = $this->account->info($_SESSION['idsession']);
            $editError = null;
        } else {
            $viewName = 'welcome.php';
        }
        include_once('../App/Views/' . $viewName);
    }

    public function info()
    {
        if (isset($_SESSION['idsession'])) {
            try {
                $viewName = 'account/info.php';
                $data = $this->account->info($_SESSION['idsession']);
            } catch (RequestException $e) {
                if ($e->hasResponse()) {
                    $responseBody = json_decode($e->getResponse()->getBody());
                    $error = $responseBody->errors[0];
                }
            }
        } else {
            $viewName = 'welcome.php';
        }
        include_once('../App/Views/' . $viewName);
    }

    public function linkGeneration()
    {
        if (isset($_SESSION['idsession'])) {
            $link = $_POST['affiliate_link'];
            if ($position = strpos($link, '?')) {
                $link = substr($link, 0, $position);
            }
            try {
                $data = $this->account->linkGeneration($_SESSION['idsession'], $link);
            } catch (RequestException $e) {
                http_response_code($e->getCode());
                if ($e->hasResponse()) {
                    $responseBody = json_decode($e->getResponse()->getBody());
                    echo $responseBody->errors[0];
                }
                return;
            }
        } else {
            $data = null;
        }
        echo $data['affiliate_link_utm'];
    }
}
